1. MEMBUAT DATABASE
CREATE DATABASE myshop;
SHOW DATABASES;
USE myshop;

2. MEMBUAT TABLE DI DALAM DATABASE

# Table users
CREATE TABLE users (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    PRIMARY KEY (id)
);

# Table items
CREATE TABLE items (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

# Table categories
CREATE TABLE categories (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    PRIMARY KEY (id)
);

3. MEMASUKKAN DATA PADA TABLE

# Table users
INSERT INTO users VALUES (null, "John Doe", "john@doe.com", "john123"),(null, "Jane Doe", "jane@doe.com", "jenita123");

# Table categories
INSERT INTO categories VALUES (null, “gadget”), (null, "cloth"), (null, "men"), (null, "women"), (null, "branded");

# Table items
INSERT INTO items VALUES (null, "Sumsang B50", "Hape keren dari merk Sumsang", 4000000, 100, 1), (null, "Uniklooh", "Baju keren dari brand ternama", 500000, 50, 2), (null, "IMHO Watch", "Jam tangan anak yang jujur banget", 2000000, 10, 1);

4. MENGAMBIL DATA DARI DATABASE

# mengambil data users KECUALI password nya
SELECT id, name, email FROM users;

# mengambil data items memiliki harga di atas 1000000 (satu juta).
SELECT FROM items 
WHERE price > 1000000;

# mengambil data items memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
SELECT * FROM items
WHERE name LIKE 'sang%';

# Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name
FROM items
INNER JOIN categories ON items.category_id = categories.id;

5. MENGUBAH DATA DARI DATABASE

# ubah data items nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5
UPDATE items
SET price = 2500000
WHERE id = 1;



